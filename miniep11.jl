using Unicode
function palindromo(palavra::String)
    if length(palavra)==0
        return true
    end
    palavra=lowercase(palavra)
    palavra=Unicode.normalize(palavra,stripmark=true)
    resp=""
    for el in palavra
        if el>='a' && el<='z'
            resp*=el
        end
    end
    if resp==reverse(resp)
        return true
    else
        return false
    end
end

using Test
function test()
    @test palindromo("Ele")
    @test palindromo("")
    @test palindromo("E l E")
    @test palindromo("ele!")
    @test !palindromo("sorvete")
    @test !palindromo("Passei em MAC0110!")
    @test palindromo("ovo")
    @test !palindromo("MiniEP11")
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!")
    @test palindromo("A mãe te ama.")
    println("Final dos testes")
end
test()